"""ШАПКА"""

import pygame
import math
import time
import random
import sys



WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
GREY  = (128, 128, 128)

WIDTH = 1920
HEIGHT = 1080
FPS = 60


pygame.init()
pygame.mixer.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))

"""Создание игрока"""

player_img = pygame.image.load(r"C:\Users\Admin\Desktop\program_lessons\wolfenstain\wolfensteinV\im.png").convert()

class Player(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = player_img
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.rect.center = (WIDTH / 2, HEIGHT / 2)
        self.rect.x = 1700
        self.rect.y = 950
        self.x_speed = 0
        self.y_speed = 0
        self.walls = None


    def changespeed(self):

        self.rect.x += self.x_speed
        self.rect.y += self.y_speed


    def update(self):

        self.rect.x += self.x_speed

        block_hit_list = pygame.sprite.spritecollide(self, self.walls, False)
        for block in block_hit_list:

            if self.x_speed > 0:
                self.rect.right = block.rect.left
            else:
                self.rect.left = block.rect.right

        self.rect.y += self.y_speed

        block_hit_list = pygame.sprite.spritecollide(self, self.walls, False)
        for block in block_hit_list:

            if self.y_speed > 0:
                self.rect.bottom = block.rect.top
            else:
                self.rect.top = block.rect.bottom
