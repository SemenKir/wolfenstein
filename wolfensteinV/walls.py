"""ШАПКА"""

import pygame
import math
import time
import random
import sys



WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
GREY  = (128, 128, 128)

WIDTH = 1920
HEIGHT = 1080
FPS = 60

pygame.init()
pygame.mixer.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))

"""Создание стен"""

class Wall(pygame.sprite.Sprite):

    def __init__(self, x, y, width, height):

        super().__init__()

        self.image = pygame.Surface([width, height])
        self.image.fill(BLUE)

        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.x = x


class Wall_Company():
    def __init__(self, wall_list, all_sprites):
        self.wall_list = wall_list
        self.all_sprites  = all_sprites



    def lvl1(self):
        for i in range(50, 1800):
            wall = Wall(i, 50, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

        for i in range(50, 1800):
            wall = Wall(i, 1080 - 50, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

        for i in range(50, HEIGHT - 50):
            wall = Wall(50, i, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

        for i in range(50, HEIGHT - 50):
            wall = Wall(1800, i, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

        for i in range(300, 700):
            wall = Wall(500, i, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

        for i in range(500, 1200):
            wall = Wall(i, 700, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

        for i in range(300, 700):
            wall = Wall(1200, i, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

        for i in range(850, 1200):
            wall = Wall(i, 300, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

        for i in range(300, 500):
            wall = Wall(850, i, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

    def game_over_collide_walls(self):
        for i in range(200, 350):
            wall = Wall(i, 200, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

        for i in range(200, 350):
            wall = Wall(350, i, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

        for i in range(200, 350):
            wall = Wall(i, 350, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

        for i in range(200, 350):
            wall = Wall(200, i, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

        for i in range(50, 1800):
            wall = Wall(i, 1080 - 50, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

        for i in range(50, 1800):
            wall = Wall(i, 50, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

        for i in range(50, HEIGHT - 50):
            wall = Wall(50, i, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

        for i in range(50, HEIGHT - 50):
            wall = Wall(1800, i, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)



    def lvl2(self):
        for i in range(50, 1800):
            wall = Wall(i, 1080 - 50, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

        for i in range(50, 1800):
            wall = Wall(i, 50, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

        for i in range(50, 1030):
            wall = Wall(50, i, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

        for i in range(50, 1030):
            wall = Wall(1800, i, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)




class Wall_PVE:

    def __init__(self, wall_list, all_sprites):
        self.wall_list = wall_list
        self.all_sprites  = all_sprites

    def PVE_LVL1(self):
        for i in range(50, 1800):
            wall = Wall(i, 1080 - 50, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

        for i in range(50, 1800):
            wall = Wall(i, 50, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

        for i in range(50, 1030):
            wall = Wall(50, i, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)

        for i in range(50, 1030):
            wall = Wall(1800, i, 10, 10)
            self.wall_list.add(wall)
            self.all_sprites.add(wall)



